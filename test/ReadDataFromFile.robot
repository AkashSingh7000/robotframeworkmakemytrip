*** Settings ***
Library    SeleniumLibrary
Library    DataDriver    ../data/testData.xlsx    sheet_name=Sheet1
Test Template    Read Data From Test Data

*** Test Cases ***
TC - Read Data From Excel
    Read Data From Test Data

*** Keywords ***
Read Data From Test Data
    Open Browser    https://www.google.com/    chrome
    Maximize Browser Window
    Sleep    10
    [Arguments]    ${searchText}
    Input Text    //textarea[@type='search']    ${searchText}
