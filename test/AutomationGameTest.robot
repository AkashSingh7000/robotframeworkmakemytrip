*** Settings ***
Library    SeleniumLibrary
Resource    ../src/pages/pageActions/makeMyTripHomePageAction.robot
Resource    ../src/pages/pageActions/hotelSearchResultsActions.robot

*** Test Cases ***
TC - Hotel Booking from Make My Trip
    Launch Browser With MakeMyTrip Website
    Click On Hotels For Booking
    Enter The Location
    Enter CheckIn Date
    Enter CheckOut Date
    Enter Room And Adults Details
    Click On Search
    Verify Filter Search Filter And Total No Of Properties
    Select MMT Luxe
    Select Taj Bengal Hotel
    Verify New Tab And Book Now