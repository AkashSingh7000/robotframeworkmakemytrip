*** Settings ***
Library    SeleniumLibrary
Resource    ../pageLocators/makeMyTripHomePageLocators.robot

*** Keywords ***
Launch Browser With MakeMyTrip Website
    Open Browser    ${url}    ${browser}
    Maximize Browser Window
    Sleep    5
    
Click On Hotels For Booking
    Run Keyword And Ignore Error    Wait Until Page Contains    ${close}
    Select Frame    id:${frameId}
    Click Element    ${close}
    Set Selenium Implicit Wait    5

Enter The Location
    Click Element    ${hotel}
    Click Element    ${locationInputElement}
    Set Selenium Implicit Wait    5
    Input Text    ${locationInputArea}    ${location}
    Run Keyword And Ignore Error    Wait Until Page Contains    ${locationElement}
    Click Element    ${locationElement}

Enter CheckIn Date
    Run Keyword And Ignore Error    Scroll Element Into View    ${checkInDate}
    Click Element    ${checkInDate}

Enter CheckOut Date
    Run Keyword And Ignore Error    Scroll Element Into View    ${checkOutDate}
    Click Element    ${checkOutDate}

Enter Room And Adults Details
    Click Element    ${roomDropDown}
    Click Element    ${roomNo}
    Click Element    ${adultDropDown}
    Click Element    ${adultNo}
    Click Element    ${childDropDown}
    Click Element    ${childNo}
    Click Element    ${childAgeDropDown}
    Run Keyword And Ignore Error    Scroll Element Into View    ${childAge}
    Click Element    ${childAge}
    Run Keyword And Ignore Error    Scroll Element Into View    ${apply}
    Click Element    ${apply}

Click On Search
    Click Element    ${search}