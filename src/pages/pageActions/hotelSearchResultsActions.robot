*** Settings ***
Library    SeleniumLibrary
Resource    ../pageLocators/hotelSearchResultsLocators.robot

*** Keywords ***
Verify Filter Search Filter And Total No Of Properties
    Run Keyword And Ignore Error    Scroll Element Into View    ${searchFilter}
    Element Should Be Visible    ${searchFilter}
    Element Should Be Visible    ${properties}

Select MMT Luxe
    Run Keyword And Ignore Error    Scroll Element Into View    ${filterMMT}
    Click Element    ${filterMMT}
    Set Selenium Implicit Wait    5

Select Taj Bengal Hotel
    Run Keyword And Ignore Error    Scroll Element Into View    ${tajHotel}
    Click Element    ${tajHotel}

Verify New Tab And Book Now
    ${newTab}=    Switch Window    NEW
    Set Selenium Implicit Wait    5
    Page Should Contain Element    ${verifyBookNow}