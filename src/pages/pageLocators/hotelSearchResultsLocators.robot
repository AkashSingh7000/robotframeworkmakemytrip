*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${searchFilter}    //span[text()='Select Filters']
${properties}    //div[@id='seoH1DontRemoveContainer']
${filterMMT}    //span[text()='MMT Luxe Selections']
${tajHotel}    //span[text()='Taj Bengal, Kolkata']
${verifyBookNow}    //button[text()='BOOK THIS NOW']