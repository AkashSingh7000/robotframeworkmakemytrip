*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${url}    https://www.makemytrip.com/
${browser}    chrome
${frameId}    webklipper-publisher-widget-container-notification-frame
${close}    //a[@class='close']
${hotel}    (//span[text()='Hotels'])[1]
${locationInputElement}    //input[@id='city']
${locationInputArea}    //input[@title='Where do you want to stay?']
${location}    Kolkata
${locationElement}    (//b[text()='Kolkata'])[1]
${roomDropDown}    (//div[@data-testid="gstSlct"])[1]
${roomNo}    //li[text()='2']
${adultDropDown}    (//div[@data-testid="gstSlct"])[2]
${adultNo}    //li[text()='4']
${childDropDown}    (//div[@data-testid="gstSlct"])[3]
${childNo}    //li[text()='1']
${checkInDate}    //div[@aria-label='Sat Dec 23 2023']
${checkOutDate}    //div[@aria-label='Mon Dec 25 2023']
${childAgeDropDown}    //span[text()='Select']
${childAge}    //li[text()='11']
${apply}    //button[text()='Apply']
${search}    //button[text()='Search']